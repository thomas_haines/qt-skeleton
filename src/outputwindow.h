#pragma once

#include <QPlainTextEdit>
#include <QObject>
#include <QTimer>

QT_BEGIN_NAMESPACE
class QWidget;
QT_END_NAMESPACE

class OutputWindow : public QPlainTextEdit {
 Q_OBJECT

 public:
  explicit OutputWindow(QWidget *parent = nullptr);

 public slots:
  /////////////////////////////////////////////////////////////
  /// Signal to print line to top of console app
  /////////////////////////////////////////////////////////////
  void appendLine(QString line);

  Q_SLOT void connected();

};
