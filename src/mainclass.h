#pragma once

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QObject>

class MainClass : public QObject {
  Q_OBJECT
 private:
  QCoreApplication *app;

 public:
  explicit MainClass(QObject *parent = 0);

 signals:
  void printIt(const QString& line);

 public slots:
  /////////////////////////////////////////////////////////////
  /// This is the slot that gets called from main to start app logic
  /////////////////////////////////////////////////////////////
  void run();

};
