
#include <QtWidgets>

#include "outputwindow.h"
#include "mainclass.h"

int main(int argv, char **args) {
  QApplication app(argv, args);

  OutputWindow outputWindow;
  outputWindow.setWindowTitle(QObject::tr("Basic App"));
  outputWindow.show();

  // create the main class
  MainClass myMain;

  // signal printIt calls outputWindow for on-screen and console output
  QObject::connect(&myMain, SIGNAL(printIt(const QString&)), &outputWindow, SLOT(appendLine(const QString&)));

  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(100, &myMain, SLOT(run()));

  return QApplication::exec();
}

