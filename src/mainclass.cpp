#include "mainclass.h"
#include <QDebug>

MainClass::MainClass(QObject *parent) : QObject(parent) {
  app = QCoreApplication::instance();
}

void MainClass::run() {
  emit printIt("run method completed");
}
