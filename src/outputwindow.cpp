
#include <QtWidgets>
#include <iostream>

#include "outputwindow.h"

OutputWindow::OutputWindow(QWidget *parent) : QPlainTextEdit(parent) {
  setReadOnly(true);
  QTimer::singleShot(3000, this, SLOT(connected()));
}

Q_SLOT void OutputWindow::connected() {
  QString line("signals connected!");
  appendLine(line);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "performance-unnecessary-value-param"
void OutputWindow::appendLine(QString line) {
  std::cout << line.toStdString() << std::endl;

  QString updatedContent = line + "\n" + this->toPlainText();
  this->setPlainText(updatedContent);
}
#pragma clang diagnostic pop

